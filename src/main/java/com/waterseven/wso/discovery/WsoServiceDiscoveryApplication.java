package com.waterseven.wso.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class WsoServiceDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsoServiceDiscoveryApplication.class, args);
	}

}
